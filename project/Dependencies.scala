import sbt._

object Dependencies {
  lazy val scalaTest = Seq("org.scalatest" %% "scalatest" % "3.0.3")
  lazy val cats = Seq("org.typelevel" %% "cats-core" % "1.0.0-MF")

  val dependencies = scalaTest ++ cats
}

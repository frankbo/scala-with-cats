package catsexercises

import cats.{Eq, Show}
import cats.instances.string._
import cats.instances.int._
import cats.syntax.eq._

final case class Cat(name: String, age: Int, color: String)
object exercise1 {
  // 1.3
  trait Writer[A] {
    def write(value: A): String
  }

  object WriterInstance {
    implicit val stringWriter: Writer[String] =
      new Writer[String] {
        override def write(value: String): String = value
      }
  }

  object WriterSyntax {
    implicit class WriterOps[A](value: A) {
      def write(implicit w: Writer[A]): String =
        w.write(value)
    }
  }

  // 1.4.6
  implicit val catShow: Show[Cat] =
    Show.show(cat => s"${cat.name} is my cat")

  // 1.5.5
  implicit val catEq: Eq[Cat] = {
    Eq.instance[Cat] { (cat1, cat2) =>
      cat1.name === cat2.name &&
      cat1.age === cat2.age &&
      cat1.color === cat2.color
    }
  }
}

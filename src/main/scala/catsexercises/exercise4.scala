package catsexercises

object exercise4 {
  trait Monad[F[_]] {
    def pure[A](a: A): F[A]
    def flatMap[A, B](value: F[A])(func: A => F[B]): F[B]
    // 4.1.2
    def map[A, B](value: F[A])(func: A => B): F[B] =
      flatMap(value)(v => pure(func(v)))
  }

  type Id[A] = A
  def idPure[A](value: A): Id[A] = value
  def idMap[A, B](initial: Id[A])(func: A => B): Id[B] = func(initial)
  def idFlatMap[A, B](initial: Id[A])(func: A => Id[B]): Id[B] = func(initial)


  implicit val monad: Monad[Id] =
    new Monad[Id] {
      override def pure[A](a: A): Id[A] = a

      override def flatMap[A, B](value: Id[A])(func: A => Id[B]): Id[B] =
        func(value)

      override def map[A, B](value: Id[A])(func: A => B): Id[B] =
        func(value)
    }

  // TODO 4.6.5
}

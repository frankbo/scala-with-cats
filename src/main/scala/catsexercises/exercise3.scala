package catsexercises

import cats.Functor

object exercise3 {
  // 3.5.4
  sealed trait Tree[+A]
  final case class Branch[A](left: Tree[A], right: Tree[A]) extends Tree[A]
  final case class Leaf[A](value: A) extends Tree[A]

  def branch[A](left: Tree[A], right: Tree[A]): Tree[A] =
    Branch(left, right)
  def leaf[A](value: A): Tree[A] =
    Leaf(value)
  implicit val treeFunctor: Functor[Tree] =
    new Functor[Tree] {
      def map[A, B](tree: Tree[A])(func: A => B): Tree[B] = tree match {
        case Branch(l, r) => Branch(map(l)(func), map(r)(func))
        case Leaf(value)  => Leaf(func(value))
      }
    }

  // 3.6.1.1
  trait Printable[A] {
    def format(value: A): String
    def contramap[B](func: B => A): Printable[B] =
      new Printable[B] {
        def format(value: B): String =
          Printable.this.format(func(value))
      }
  }

  def format[A](value: A)(implicit p: Printable[A]): String =
    p.format(value)

  implicit val booleanPrintable: Printable[Boolean] =
    new Printable[Boolean] {
      def format(value: Boolean): String =
        if (value) "yes" else "no"
    }

  final case class Box[A](value: A)

  implicit def boxPrintable[A](implicit p: Printable[A]) =
    p.contramap[Box[A]](_.value)

  // TODO 3.6.2.1

}

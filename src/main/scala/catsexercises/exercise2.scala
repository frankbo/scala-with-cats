package catsexercises

import cats.{Monoid => M}
import cats.syntax.semigroup._ // for |+|

object exercise2 {
  trait Semigroup[A] {
    def combine(x: A, y: A): A
  }
  trait Monoid[A] extends Semigroup[A] {
    def empty: A
  }
  object Monoid {
    def apply[A](implicit monoid: Monoid[A]): Monoid[A] =
      monoid
  }

  // 2.3
  implicit val booleanAndMonoid: Monoid[Boolean] =
    new Monoid[Boolean] {
      def combine(a: Boolean, b: Boolean): Boolean = a && b
      def empty = true
    }

  implicit val booleanOrMonoid: Monoid[Boolean] =
    new Monoid[Boolean] {
      def combine(a: Boolean, b: Boolean): Boolean = a || b
      def empty = false
    }

  implicit val booleanEitherMonoid: Monoid[Boolean] =
    new Monoid[Boolean] {
      def combine(a: Boolean, b: Boolean): Boolean =
        (a && !b) || (!a && b)
      def empty = false
    }

  implicit val booleanXnorMonoid: Monoid[Boolean] =
    new Monoid[Boolean] {
      def combine(a: Boolean, b: Boolean): Boolean =
        (!a || b) && (a || !b)
      def empty = true
    }

  // 2.4
  implicit def list[A]: Monoid[Set[A]] =
    new Monoid[Set[A]] {
      override def empty: Set[A] = Set.empty[A]

      override def combine(x: Set[A], y: Set[A]): Set[A] = x ++ y
    }

  // 2.5.4
  def sumInt(items: List[Int]): Int = items.sum

  def sumSemigroup[A: M](items: List[A]): A =
    items.foldLeft(M[A].empty)(_ |+| _)

  case class Order(totalCost: Double, quantity: Double)
  implicit val orderMonoid: Monoid[Order] = {
    new Monoid[Order] {
      override def empty: Order = Order(0.0, 0.0)

      override def combine(x: Order, y: Order): Order =
        Order(x.totalCost + y.totalCost, x.quantity + y.quantity)
    }
  }
}

package catsexercises

import cats.syntax.show._
import exercise1.WriterInstance._
import exercise1.WriterSyntax._
import exercise1._

import org.scalatest.{FlatSpec, Matchers}

class exercise1Spec extends FlatSpec with Matchers {
  it should "print hello" in {
    "Hello".write shouldEqual "Hello"
  }

  it should "print the kitty" in {
    Cat("kitty", 22, "yellow").show shouldEqual "kitty is my cat"
  }

  it should "compare two cats" in {
    val cat1 = Cat("kitty", 22, "yellow")
    val cat2 = Cat("kitty", 22, "yellow")

    cat1 === cat2 shouldBe true
  }
}

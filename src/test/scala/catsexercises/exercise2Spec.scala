package catsexercises

import exercise2._
import exercise2.list
import cats.instances.string._
import org.scalatest.{FlatSpec, Matchers}

class exercise2Spec extends FlatSpec with Matchers {
  it should "return an empty set" in {
    Monoid[Set[Int]].empty shouldEqual Set.empty
  }

  it should "combine two sets" in {
    Monoid[Set[Int]].combine(Set(1, 2, 3), Set(4)) shouldEqual Set(1, 2, 3, 4)
  }

  it should "sum all elements that are integers" in {
    sumInt(List(1, 2, 3)) shouldEqual 6
  }

  it should "sum all elements that have an `add` method" in {
    sumSemigroup(List("hello", " world")) shouldEqual "hello world"
  }

  it should "return an empty Order" in {
    Monoid[Order].empty shouldEqual Order(0.0, 0.0)
  }

  it should "combine two Orders" in {
    Monoid[Order].combine(Order(1, 2), Order(1, 2)) shouldEqual Order(2.0, 4.0)
  }
}

package catsexercises

import exercise4._
import org.scalatest.{FlatSpec, Matchers}

class exercise4Spec extends FlatSpec with Matchers {

  "Id" should "should return initial value on running pure" in {
    val result: Id[Int] = 123
    idPure(123) shouldEqual result
  }

  it should "should map over an Id" in {
    val result: Id[Int] = 124
    idMap(123)(_ + 1) shouldEqual result
  }

  it should "should flatMap over an Id" in {
    val result: Id[Int] = 124
    idFlatMap(123)(_ + 1) shouldEqual result
  }

}

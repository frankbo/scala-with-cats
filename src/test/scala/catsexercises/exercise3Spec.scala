package catsexercises

import exercise3._
import cats.syntax.functor._
import org.scalatest.{FlatSpec, Matchers}

class exercise3Spec extends FlatSpec with Matchers {
  it should "call a function on every element in the tree" in {
    val tree =
      branch(branch(leaf(2), leaf(4)), leaf(1)).map(_ + 1)

    tree shouldEqual Branch(Branch(Leaf(3), Leaf(5)), Leaf(2))
  }

  it should "return yes for a truthy format" in {
    format(true) shouldEqual "yes"
  }

  it should "return yes for a truthy boxed format" in {
    format(Box(true)) shouldEqual "yes"
  }
}
